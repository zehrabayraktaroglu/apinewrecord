﻿using HareketApi.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HareketApi.Controllers
{
    public class HareketController : Controller
    {
        private IHareketService _hareketService;

        public HareketController(IHareketService hareketService)
        {
            _hareketService = hareketService;
        }
        [HttpPost]
        [Route("/v1/hareket/start")]
        public async Task<IActionResult> StartAsync([FromBody] HareketInputModel input)
        {
            await _hareketService.PlayAsync(input);
            return Ok(true);
        }
        [HttpPost]
        [Route("/v1/hareket/pause")]
        public async Task<IActionResult> PauseAsync([FromBody] HareketInputModel input)
        {
            await _hareketService.PauseAsync(input);
            return Ok(true);
        }
        [HttpPost]
        [Route("/v1/hareket/stop")]
        public async Task<IActionResult> StopAsync([FromBody] HareketInputModel input)
        {
            await _hareketService.StopAsync(input);
            return Ok(true);
        }
    }
}
