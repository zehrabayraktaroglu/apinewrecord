﻿using RestSharp;
using SetCrmHelper.MayaModels;
using System;
using System.Text.Json;
using System.Threading.Tasks;

namespace SetCrmHelper
{
    public class SetCRMHelper : ISetCrmHelper
    {
        public async Task<RecordResponse> PostRecordAsync(RecordRequestParameters input)
        {
            var client = new RestClient("https://demoevimapi.indata.com.tr/v1/record");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "6B4E7847BE104C6AB4995F831E1F6F6F");
            request.AddHeader("Content-Type", "application/json");
            var body = JsonSerializer.Serialize(input);

            request.AddParameter("application/json", body, ParameterType.RequestBody);
            var response = client.Execute(request);

            if (response.IsSuccessful)
            {
                var result = JsonSerializer.Deserialize<RecordResponse>(response.Content);
                return result;
            }
            else
            {
                throw new Exception($"API Post Record Failed! [REQUEST:{body}]");
            }         
        }
    }
}
